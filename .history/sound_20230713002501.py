import pygame as pg

class Sound:
    def __init__(self, game):
        self.game = game
        pg.mixer.init()
        self.path = 'resources/sound/'
        self.shotgun = pg.mixer.Sound(self.path + 'shotgun.ogg')
        self.npc_pain = pg.mixer.Sound(self.path + 'npc_pain.ogg')
        self.npc_death = pg.mixer.Sound(self.path + 'npc_death.ogg')
        self.npc_shot = pg.mixer.Sound(self.path + 'npc_attack.ogg')
        self.player_pain = pg.mixer.Sound(self.path + 'player_pain.ogg')
        self.theme = pg.mixer.Sound(self.path + 'theme.mp3')