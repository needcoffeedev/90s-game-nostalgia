import pygame as pg

_ = False
mini_map = [
    [2, 11, 2, 2, 11, 2, 7, 8, 9, 10, 2, 2, 2, 11, 2, 11, 2],
    [2, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, 2],
    [2, _, _, 1, 1, 1, 1, _, _, _, _, 3, 4, 3, _, _, 2],
    [10, _, _, _, _, _, 12, 1, _, _, _, _, _, 5, _, _,7],
    [9, _, _, _, _, _, 13, 1, _, _, _, _, _, 3, _, _, 8],
    [8, _, _, 1, 1, 1, 1, _, _, _, _, _, _, _, _, _, 9],
    [7, _, _, _, _, _, _, _, _, _, _, _, _, _, _, _, 10],
    [2, _, _, 1, _, _, _, 1, _, _, _, _, _, _, _, 2],
    [2, 11, 2, 6, 6, 6, 6, 6, 2, 2, 2, 2, 11, 2, 11, 2],
]

class Map:
    def __init__(self, game):
        self.game = game
        self.mini_map = mini_map
        self.world_map = {}
        self.rows = len(self.mini_map)
        self.cols = len(self.mini_map[0])
        self.get_map()

    def get_map(self):
        for j, row in enumerate(self.mini_map):
            for i, value in enumerate(row):
                if value:
                    self.world_map[(i,j)] = value

    def draw(self):
        [pg.draw.rect(self.game.screen, 'darkgray', (pos[0] * 100, pos[1] * 100, 100, 100), 2)
        for pos in self.world_map]  
